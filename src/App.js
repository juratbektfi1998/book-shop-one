import React from 'react';
import './App.scss';
import Layout from "./layout/Layout";
import {Route, Switch} from "react-router-dom";
import Pay from "./pages/pay/Pay";
import NotFound from "./pages/not-found/NotFound";
import Landing from "./pages/landing/Landing";
import Admin from "./pages/admin/Admin";
import ChangePassword from "./pages/change-password/ChangePassword";
import PrivateRoute from "./components/PrivateRoute";
import Login from "./pages/login/Login";
import Comments from "./pages/comments/Comments";

function App() {

    return (
        <Layout className="App">
          <Switch>
              <Route exact path={'/pay'} component={Pay}/>
              <PrivateRoute path={'/admin'} component={Admin}/>
              <PrivateRoute path={'/change'} component={ChangePassword}/>
              <PrivateRoute path={'/comments'} component={Comments}/>
              <Route exact path={'/login'} component={Login}/>
              <Route exact path={'/'} component={Landing}/>
              <Route component={NotFound}/>
          </Switch>
        </Layout>
    );
}

export default App;
