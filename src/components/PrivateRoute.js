import React from 'react';
import {Redirect, Route} from "react-router-dom";

const PrivateRoute = ({to, component}) => {
    const token = localStorage.getItem('nanoToken');
    return token ? (
        <Route exact path={to} component={component}/>
    ) : (
        <Redirect to={'/login'}/>
    )
};

export default PrivateRoute;
