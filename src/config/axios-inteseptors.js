import axios from "axios";
import {BASE_URL} from "./constants";

const client = axios.create({
    baseURL: BASE_URL,
    headers: {
        Accept: "*/*",
        "Content-Type": "application/json",
    },
    responseType: "application/json",
});

client.interceptors.request.use(
    (config) => {
        const token = localStorage.getItem('nano-token');
        if (token) {
            config.headers.authorization = `Bearer ${token}`;
        }
        return config;
    },
    (error) => {
        return Promise.reject(error);
    }
);

client.interceptors.response.use(
    function (response) {
        return response;
    },
    function (err) {
        const status = err.status || err.response.status;
        if (status === 401 || status === 403) {
            localStorage.removeItem('nano-token');
            window.location.replace('/login')
        }
        return Promise.reject(err);
    }
);

export default client;
