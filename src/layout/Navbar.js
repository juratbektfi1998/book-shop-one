import React, {useEffect, useState} from 'react';
import {Link, useLocation} from 'react-router-dom';
import logo from 'images/logo-removebg-preview.png'

const Navbar = () => {
    const [color, setColor] = useState('bg-transparent');
    const [item, setItem] = useState(false);
    const {pathname} = useLocation();

    useEffect(() => {
        if (pathname === '/' || pathname === '/#about') {
            window.addEventListener('scroll', () => {
                if (window.scrollY >= 200) {
                    setColor('bg-light')
                } else {
                    setColor('bg-transparent')
                }
            });
        } else {
            setColor('bg-light')
        }
        return () => {
            window.removeEventListener('scrollY', () => {
                setColor('bg-transparent')
            })
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        setItem(!item)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [pathname]);

    const getItem = () => {
        switch (pathname) {
            case '/':
            case '/#about':
                return (<a className="nav-link active" aria-current="page" href={'#about'}>Kitob haqida</a>);
            case '/pay':
                return null;
            case '/admin':
                return (<>
                    <li className="nav-item">
                        <Link className="nav-link active" aria-current="page" to="/change">Sozlamalar</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link active" aria-current="page" to="/comments">Sharxlar</Link>
                    </li>
                </>);
            case '/comments':
                return (<>
                    <li className="nav-item">
                        <Link className="nav-link active" aria-current="page" to="/admin">Admin panel</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link active" aria-current="page" to="/change">Sozlamalar</Link>
                    </li>
                </>);
            case '/change':
                return (<>
                    <li className="nav-item">
                        <Link className="nav-link active" aria-current="page" to="/admin">Admin panel</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link active" aria-current="page" to="/comments">Sharxlar</Link>
                    </li>
                </>);
            default:
                return null
        }
    };

    return (
        <>
            <nav className={`navbar navbar-expand-lg navbar-light ${color}`}>
                <div className="container">
                    <Link className="navbar-brand" to="/#">
                        <img src={logo} width={50} alt=""/> Nano Book</Link>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                            data-bs-target="#navbarNavDropdown"
                            aria-controls="navbarNavDropdown"
                            data-toggle="collapse"
                            data-target="#navbarSupportedContent"
                            aria-expanded="false"
                            aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"/>
                    </button>
                    <div className="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
                        <ul className="navbar-nav">
                            {/*<li className="nav-item">*/}
                            {/*    <Link className="nav-link active" aria-current="page" to="/admin">Admin panel</Link>*/}
                            {/*</li>*/}
                            <li className="nav-item">
                                {getItem()}
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link active" aria-current="page" to="/pay">Buyurtma berish</Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </>
    );
};

export default Navbar;
