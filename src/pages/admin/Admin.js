import React, {Suspense, useEffect, useState} from 'react';
import axios from "config/axios-inteseptors";
import './admin.scss';

// const Modal = React.lazy(() => import('../../components/Modal'));

const Admin = () => {
    // const [img, setImg] = useState(null);
    const [orders, setOrders] = useState([]);
    const [loading, setLoading] = useState(true);

    // const getUrl = (response) => {
    //     const urlCreator = window.URL || window.webkitURL;
    //     return urlCreator.createObjectURL(response);
    // };

    // const getImage = (id) => {
    //     axios.get(`file/order/${id}`, {responseType: 'blob'})
    //         .then(res => {
    //             let imageUrl = getUrl(res.data);
    //             setImg(imageUrl)
    //         })
    //         .catch(err => alert(err))
    // };


    useEffect(() => {
        axios.get('order/all')
            .then(res => {
                setOrders(res.data);
                setLoading(false)
            })
            .catch(err => {
                alert(err);
                setLoading(false)
            })
    }, []);

    const changeStatus = (e, id) => {
        axios.put(`order/state/${id}/${e.target.value}`)
            .then(res => alert(res.data.message))
            .catch(err => alert(err));
    };

    if (loading) return <h3>Loading...</h3>;
    return (
        <div className="container admin">
            <Suspense fallback={<p>Loading...</p>}>
                {/*<Modal img={img}/>*/}
            </Suspense>
            <div className="row">
                <div className="col-md-12 col-10 offset-1 offset-md-0 border border-radius overflow-x">
                    <table className="table overflow-x text-center table-striped w-100">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Ismi</th>
                            <th>Address</th>
                            <th>Narxi</th>
                            <th>Sanasi</th>
                            {/*<th>Chek</th>*/}
                            <th>Holati</th>
                        </tr>
                        </thead>
                        <tbody>
                        {orders.length > 0 ? orders.map((order, key) => (
                            <tr key={order['id']}>
                                <th>{key + 1}</th>
                                <td>{order['fullName']}</td>
                                <td>{order['address']}</td>
                                <td>{order['sum']}</td>
                                <td>{order['created']}</td>
                                {/*<td>*/}
                                {/*    <button onClick={() => getImage(order['id'])} className="btn btn-primary"*/}
                                {/*            data-toggle="modal"*/}
                                {/*            data-target="#exampleModal">Ko`rish*/}
                                {/*    </button>*/}
                                {/*</td>*/}
                                <td>
                                    <select name="status" id="" defaultValue={order['stateId']}
                                            onChange={(e) => changeStatus(e, order['id'])}
                                            className="btn btn-success">
                                        <option value="1">Yangi</option>
                                        <option value="2">Jarayonda</option>
                                        <option value="3">Muvaffaqiyatli</option>
                                        <option value="4">Rad etilgan</option>
                                    </select>
                                </td>
                            </tr>
                        )) : (<h1 className="text-center">Buyurtmalar mavjud emas</h1>)}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
};

export default Admin;
