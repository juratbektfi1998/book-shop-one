import React from 'react';
import {useForm} from "react-hook-form";
import axios from "config/axios-inteseptors";
import './ChangePassword.scss';

const ChangePassword = () => {
    const {register, handleSubmit, errors} = useForm();

    const changePassword = values => {
        axios.put(`card-number?cardNumber=${values['card_number']}`)
            .then(res => alert(res.data.message))
            .catch(err => alert(err))
    };

    const changeCardNumber = values => {
        axios.put(`card-number?cardNumber=${values['card_number']}`)
            .then(res => alert(res.data.message))
            .catch(err => alert(err))
    };

    const changeSum = (values) => {
        axios.put(`price?sum=${values['sum']}`)
            .then(res => alert(res.data.message))
            .catch(err => alert(err))
    };

    return (
        <div className="justify-body-center mt-md-5 pt-md-5">
            <div className="border border-radius change-form">
                <form action="" onSubmit={handleSubmit(changeCardNumber)}>
                    <input className="form-control" type="text" placeholder="Plastik karta raqami"
                           name={'card_number'}
                           aria-label=".form-control-sm example" ref={register({required: true})}/>
                    {errors.card_number && <span>This field is required!</span>}
                    <button className="btn btn-primary my-3">Karta raqamini o`zgartirish</button>
                </form>
                <form action="" onSubmit={handleSubmit(changeSum)}>
                    <input className="form-control" type="text" placeholder="Kitob narxi"
                           name={'sum'}
                           aria-label=".form-control-sm example" ref={register({required: true})}/>
                    {errors.sum && <span>This field is required!</span>}
                    <button className="btn btn-primary my-3">Kitob narxini o`zgartirish</button>
                </form>
                <form action="" onSubmit={handleSubmit(changePassword)}>
                    <input className="form-control mt-3" type="text" placeholder="Password"
                           name={'password'}
                           aria-label=".form-control-sm example" ref={register({required: true})}/>
                    {errors.password && <span>This field is required!</span>}
                    <button className="btn btn-primary my-3">Parolni o`zgartirish</button>
                </form>
            </div>
        </div>
    );
};

export default ChangePassword;
