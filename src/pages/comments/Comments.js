import React, {useState, useEffect} from 'react';
import axios from "../../config/axios-inteseptors";

const Comments = () => {
    const [comments, setComments] = useState([]);

    useEffect(() => {
        getComments()
    }, []);

    const deleteComment = (id) => {
        axios.delete(`description/${id}`)
            .then(res => {
                alert(res.message);
                getComments()
            })
            .catch(err => alert(err))
    };

    const getComments = () => {
        axios.get('info/description')
            .then(res => setComments(res.data))
            .catch(err => alert(err))
    };

    return (
        <div className="justify-body-center">
            <div className="container">
                <div className="row">
                    <div className="col-md-12 col-10 offset-1 offset-md-0 border border-radius overflow-x">
                        <h1>Ohirgi sharhlar</h1>
                        <table className="table overflow-x text-center table-striped w-100">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Ismi</th>
                                <th>Text</th>
                                <th>Sanasi</th>
                                <th/>
                            </tr>
                            </thead>
                            <tbody>
                            {comments.length > 0 ? comments.map((comment, key) => (
                                <tr key={comment['id']}>
                                    <th>{key + 1}</th>
                                    <td>{comment['author']}</td>
                                    <td>{comment['text']}</td>
                                    <td>{comment['dateCreate'].substr(0, 10)}</td>
                                    <td>
                                        <button onClick={() => deleteComment(comment['id'])}
                                                className="btn btn-primary">O`chirish
                                        </button>
                                    </td>
                                </tr>
                            )) : (<h1 className="text-center">Sharxlar mavjud emas</h1>)}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Comments;
