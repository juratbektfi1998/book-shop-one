import React from 'react';
import img0 from 'images/Ruchka.png'
import img from 'images/03-removebg-preview.png'
import img1 from 'images/02-removebg-preview.png'
import {Link} from "react-router-dom";

const AboutSection = () => {
    return (
        <section className="section about-section p-lg-0" id="about">
            <div className="row pt-md-3 pt-0">
                <div className="col-12 offset-md-1 col-md-4 offset-0"
                     data-aos="flip-right"
                     data-aos-duration="1000"
                >
                    <div className="card">
                        <div className="card-img-top">
                            <img src={img} alt="" className="about-img"/>
                        </div>
                    </div>
                </div>
                <div className="col-12 col-md-6"
                     data-aos="fade-left"
                     data-aos-duration="1000"
                >
                    <div className="card">
                        <div className="card-body">
                            <ul className="about-list">
                                <li className="d-flex  ">
                                    <div className="about-box">A</div>
                                    Nano texnologiya asosida yaratilgan metodika
                                </li>
                                <li className="d-flex  ">
                                    <div className="about-box">B</div>
                                    Ikkita tilni bitta kitobdan o’rganish imkoniyati
                                </li>
                                <li className="d-flex  ">
                                    <div className="about-box">C</div>
                                    Speaker yordamida sun’iy atmosferaga ega bo’lish imkoniyati
                                </li>
                                <li className="d-flex  ">
                                    <div className="about-box">D</div>
                                    Eng qisqa muddatda, ya’ni 3 oyda so’zlashish imkoniyati
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row mt-md-5 mt-1 flex-column-reverse flex-md-row">
                <div className="col-12 col-md-6 offset-md-1"
                     data-aos="fade-left"
                     data-aos-duration="1000"
                >
                    <div className="card">
                        <div className="card-body">
                            <ul className="about-list">
                                <li className="d-flex  ">
                                    <div className="about-box">E</div>
                                    Aqlli yondashuv – to’gri
                                    yo’naltirilgan metodika
                                </li>
                                <li className="d-flex  ">
                                    <div className="about-box">F</div>
                                    Zerikarli qoidalarsiz, qiziqarli va
                                    samarali usulda o’rganish imkoniyati
                                </li>
                                <li className="d-flex  ">
                                    <div className="about-box">G</div>
                                    Istalgan joyda va istalgan vaqtda
                                    o’rganish imkoniyati
                                </li>
                                <li className="d-flex  ">
                                    <div className="about-box">H</div>
                                    Yuqori va kafolatlangan sifat
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="col-8 offset-2 offset-md-0 col-md-4"
                     data-aos="flip-right"
                     data-aos-duration="1000"
                >
                    <div className="card">
                        <div className="card-img-top">
                            <img src={img1} alt="" className="about-img"/>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row mt-md-5 mt-2">
                <div className="col-md-4 col-6 offset-3 offset-md-1 my-offset"
                     data-aos="flip-right"
                     data-aos-duration="1000">
                    <div className="card">
                        <div className="card-img-top">
                            <img src={img0} alt="" className="about-img img0 pl-2"/>
                            <div className="card-body text-center">
                                <p>Speaker</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-12 col-md-6"
                     data-aos="fade-left"
                     data-aos-duration="1000"
                >
                    <div className="card">
                        <div className="card-body">
                            <ul className="about-list">
                                <li className="d-flex">
                                    <div className="about-box">I</div>
                                    Speaker yordamida tillarning talaffuz
                                    qoidalarini oson va samarali
                                    o'rganish imkoniyati
                                </li>
                                <li className="d-flex">
                                    <div className="about-box">J</div>
                                    O'z xotirasiga ega speaker ovoz yozib
                                    olish xususiyatiga ega bo'lib, o'z
                                    tallafuzingiz ustida ishlash imkoniyatiga ega bo'lasiz
                                </li>
                                <li className="d-flex">
                                    <div className="about-box">K</div>
                                    Speaker sizga doimiy ravishda
                                    diqqatni bir yerga jamlashga yordam beradi
                                </li>
                                <li className="d-flex">
                                    <div className="about-box">L</div>
                                    Speakerning quloqchin ulash
                                    imkoniyati undan istalgan vaqtda va istalgan
                                    joyda foydalanish imkonini beradi
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row mt-md-5 mt-sm-1 pb-3">
                <div className="col-md-4 offset-md-4">
                    <Link className="btn w-100 btn-primary" to={'/pay'}>Buyurtma berish</Link>
                </div>
            </div>
        </section>
    );
};

export default AboutSection;
