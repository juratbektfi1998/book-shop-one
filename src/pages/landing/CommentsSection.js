import React, {useEffect, useState} from 'react';
import axios from 'config/axios-inteseptors';
import {useForm} from 'react-hook-form';

const CommentsSection = () => {
    const [comments, setComments] = useState([]);
    const {register, handleSubmit, errors} = useForm();

    // useEffect(() => {
    //     getComments()
    // }, []);
    //
    const onSubmit = (values) => {
        axios.post('description', {
            author: values.author,
            text: values.text
        })
            // .then(res=>getComments())
            .catch(err=>alert(err))
    };
    //
    // const getComments = () => {
    //     axios.get('info/description')
    //         .then(res => setComments(res.data))
    //         .catch(err => alert(err))
    // };

    return (
        <div className="py-5">
            <div className="container">
                <h1 className="text-center">Sharhlar</h1>
                <div className="row">
                    {comments.map(comment => (
                        <div key={comment['id']} className="col-12">
                            <h4
                                data-aos="fade-left"
                                data-aos-duration="2000"
                            >{comment['author']}</h4>
                            <p
                                data-aos="zoom-in-up"
                                data-aos-duration="2000"
                            >{comment['text']}</p>
                            <hr/>
                        </div>
                    ))}
                </div>
                <div className="row">
                    <div className="col-12">
                        <form action="" onSubmit={handleSubmit(onSubmit)}
                              className="w-100 align-items-center p-2 d-flex flex-column">
                            <input type="text"
                                   name="author"
                                   className="form-control"
                                   ref={register({required: true})}
                                   placeholder="Ismingiz"
                                   aria-label="Ismingiz"
                                   aria-describedby="addon-wrapping"/>
                            {errors.author && <span className="text-danger">This field is required!</span>}
                            <div className="form-group mt-3 w-100">
                                <textarea className="form-control"
                                          ref={register({required: true})}
                                          name="text"
                                          placeholder="Fikringizni bu yerga yozing"
                                          id="exampleFormControlTextarea1" rows="3"/>
                                {errors.text && <span className="text-danger">This field is required!</span>}
                            </div>
                            <button className="btn btn-primary mt-2 w-100">Sharh qoldirish</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CommentsSection;
