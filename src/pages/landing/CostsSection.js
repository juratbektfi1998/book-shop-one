import React, {useState, useEffect} from 'react';
import {Link} from "react-router-dom";
import axios from "config/axios-inteseptors";

const CostsSection = () => {
    const [cost, setCost] = useState(0);

    useEffect(()=>{
        // axios.get('info/price')
        //     .then(res=>setCost(res.data))
        //     .catch(err=>alert(err))
    }, []);

    return (
        <div className="section">
            <div className="container">
                <div className="row p-3 text-center">
                    <div className="col-12">
                        <h4>HOZIROQ KITOBIMIZGA BUYURTMA BERING!</h4>
                        <h4>48 SOAT ICHIDA BUYURTMANGIZ UYINGIZDA BO`LADI!</h4>
                        <h1>O`ZBEKISTON BO`YLAB YETKAZIB BERISH</h1>
                        <Link to={'/pay'} className="btn btn-danger w-25">BEPUL!</Link>
                    </div>
                    <div className="col-md-12 mt-5">
                        <div className="shadowed-box">
                            <div className="row">
                                <div className="col-md-4">
                                    <h3>Kitob narxi:</h3>
                                    <h1>
                                        <del>{cost / 0.9}</del>
                                    </h1>
                                    <h5>so`m</h5>
                                </div>
                                <div className="col-md-4">
                                    <h3 className="text-danger">10% chegirmada:</h3>
                                    <h1 className="text-success">{cost}</h1>
                                    <h5 className="text-danger">so`m</h5>
                                </div>
                                <div className="col-md-4">
                                    <h3>Tejab qolasiz:</h3>
                                    <h1>{(cost / 0.9) - cost}</h1>
                                    <h5>so`m</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CostsSection;
