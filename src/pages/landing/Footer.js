import React from 'react';

const Footer = () => {
    return (
        <div className="footer pb-2">
            <div className="container">
                <div className="row">
                    <div className="col-md-4 flex-column">
                        <p className="m-0">Tel: +99894 489 6626</p>
                        {/*<a href="tel:+998944896626">Tel: +99894 489 6626</a>*/}
                        <p className="m-0">Tel: +99894 471 6626</p>
                    </div>
                    <div className="col-md-4">
                        {/* eslint-disable-next-line react/jsx-no-target-blank */}
                        <p><a href="https://t.me/Nanobook_uz" target="_blank">Telegram channel: NanoBook.uz</a></p>
                    </div>
                    <div className="col-md-4">
                        {/* eslint-disable-next-line react/jsx-no-target-blank */}
                        <p><a href="https://www.instagram.com/nanobook.uz/?hl=ru" target="_blank">Instagram: NanoBook.uz</a></p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Footer;
