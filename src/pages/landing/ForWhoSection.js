import React from 'react';

const ForWhoSection = () => {
    return (
        <div className="section for-who-section container">
            <h1 className="text-center font-weight-bold"
                data-aos="zoom-in"
                data-aos-duration="1000"
            >Primer kimlar uchun?</h1>
            <div className="row">
                <div className="col-md-8 offset-md-2">
                    <ul className="list">
                        <li><div className="count" data-aos="fade-right">1</div> Ingliz va Rus tillarida mukammal so’zalshishni xohlaydigan 7 yoshdan 70 yoshgacha bo’lgan keng jamoa uchun</li>
                        <li><div className="count" data-aos="fade-right">2</div> Til ko’nikmalari endi shakllanayotgan yoshlar uchun</li>
                        <li><div className="count" data-aos="fade-right">3</div> Boshlang’ich yoki o’rtacha bilimga ega, lekin so’zlashishga qiynaladigan talabalar uchun</li>
                        <li><div className="count" data-aos="fade-right">4</div> Chet elda o’qish niyatida bo’lganlar uchun</li>
                        <li><div className="count" data-aos="fade-right">5</div> Chet elda ishlab, til to’siqlariga uchrayotganlar uchun</li>
                        <li><div className="count" data-aos="fade-right">6</div> Til karyerasida asosiy to’siq bo’layotganlar uchun</li>
                        <li><div className="count" data-aos="fade-right">7</div> Til o’rganishga kechikdim deb o’ylovchi nuroniylar uchun</li>
                        <li><div className="count" data-aos="fade-right">8</div> Kursga qatnab til o’rganishga imkoniyati yo’qlar uchun</li>
                        <li><div className="count" data-aos="fade-right">9</div> Global biznes qurish niyatidagi tadbirkorlar uchun</li>
                        <li><div className="count" data-aos="fade-right">10</div> O’qituvchilar tomonindan qo’shimcha darslarda erkin foydalanish uchun</li>
                    </ul>
                </div>
            </div>
        </div>
    );
};

export default ForWhoSection;
