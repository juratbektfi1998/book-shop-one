import React, {useEffect} from 'react';
import Aos from 'aos';
import 'aos/dist/aos.css';
import MainSection from "./MainSection";
import './index.scss';
import Footer from "./Footer";
import AboutSection from "./AboutSection";
import ForWhoSection from "./ForWhoSection";
import CommentsSection from "./CommentsSection";
import StepsSection from "./StepsSection";
import CostsSection from "./CostsSection";

const Landing = () => {

    useEffect(() => {
        Aos.init({duration: 2000})
    }, []);

    return (
        <div>
            <MainSection/>
            <AboutSection/>
            <StepsSection/>
            <ForWhoSection/>
            <CostsSection/>
            <CommentsSection/>
            <Footer/>
        </div>
    );
};

export default Landing;
