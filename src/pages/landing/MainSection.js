import React from 'react';
import img from "images/main_section.png";
import {Link} from "react-router-dom";

const MainSection = () => {
    return (
        <div className="main-section justify-body-center pb-md-2 pb-0">
            <div className="container">
                <div className="row">
                    <div className="col-md-8" data-aos="fade-right">
                        <div className="card border-0 bg-transparent">
                            <div className="card-body pt-0">
                                <h2>
                                    Ingliz va Rus tillarida <b>oson</b> so'zlashish metodikasi
                                </h2>
                                <h2>
                                    Primer va nano speaker yordamida  <b>3 oyda</b> erkin gaplashing!
                                    Endi <b>2 til 1 kitobda!</b>
                                </h2>
                                <Link className="btn btn-primary mt-3 w-50 w-xs-100" to="/pay">Buyurtma berish</Link>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4" data-aos="fade-right">
                        <div className="card img border-0">
                            <img src={img || null} className="card-img-top" alt=""/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default MainSection;
