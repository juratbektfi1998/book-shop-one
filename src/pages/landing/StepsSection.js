import React from 'react';

const StepsSection = () => {
    return (
        <div className="py-5 steps-section px-5 text-center">
            <h1>Oddiy 3 qadam</h1>
            <h3>JUDA OSON SOTIB OLASIZ</h3>
            <div className="container-fluid">
                <div className="row text-center mt-5">
                    <div className="col-md-4 col-12 mb-2"
                         data-aos="zoom-in"
                         data-aos-duration={1000}>
                        <div className="box-1 mx-auto box">1</div>
                        <h3>FORMANI TO`LDIRING VA BUYURTMA BERING</h3>
                    </div>
                    <div className="col-md-4 col-12 mb-2"
                         data-aos="zoom-in"
                         data-aos-duration={1000}>
                        <div className="box-2 mx-auto box">2</div>
                        <h3>TEZ ORADA SIZ BILAN BOG`LANAMIZ</h3>
                    </div>
                    <div className="col-md-4 col-12 mb-2"
                         data-aos="zoom-in"
                         data-aos-duration={1000}>
                        <div className="box-3 mx-auto box">3</div>
                        <h3>48 SOAT ICHIDA KITOB QO`LINGIZDA BO`LADI</h3>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default StepsSection;
