import React from 'react';
import {useForm} from "react-hook-form";
import {useHistory} from 'react-router-dom';
import axios from "config/axios-inteseptors";

const Login = () => {
    const {register, handleSubmit, errors} = useForm();
    const history = useHistory();

    const onSubmit = values => {
        axios.post('auth', {
            login: values['login'],
            password: values['password']
        }).then(res => {
            localStorage.setItem('nano-token', res.data.accessToken);
            axios.interceptors.request.use((config) => {
                config.headers.authorization = `Bearer ${res.data.accessToken}`;
                return config;
            });
            history.push('/admin')
        }).catch(err => alert(err))
    };

    return (
        <div className="justify-body-center mt-2">
            <form onSubmit={handleSubmit(onSubmit)} className={'form'}>
                <input className="form-control" type="text" placeholder="Login"
                       name={'login'}
                       aria-label=".form-control-sm example" ref={register({required: true})}/>
                {errors.login && <span>This field is required!</span>}
                <input className="form-control mt-3" type="password" placeholder="Password"
                       name={'password'}
                       ref={register({required: true})}
                       aria-label=".form-control-sm example"/>
                {errors.password && <span>This field is required!</span>}
                <button className="btn btn-primary mt-3" type="submit">Kirish</button>
            </form>
        </div>
    );
};

export default Login;
