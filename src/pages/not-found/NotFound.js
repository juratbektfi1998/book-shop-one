import React from 'react';

const NotFound = () => {
    return (
        <div className="text-center justify-body-center text-danger">
            <div>
                <h1>404</h1>
                <h3>Page not found</h3>
            </div>
        </div>
    );
};

export default NotFound;
