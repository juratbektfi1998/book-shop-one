import React, {useEffect, useState} from 'react';
import {useForm} from "react-hook-form";
import './index.scss'
import axios from "config/axios-inteseptors";

const Pay = () => {
    const [file, setFile] = useState(null);
    const [cardNumber, setCardNumber] = useState(null);
    const [cost, setCost] = useState(null);
    const {register, handleSubmit, errors} = useForm();

    // useEffect(() => {
    //     axios.get('info/price')
    //         .then(res => setCost(res.data.data))
    //         .catch(err => alert(err));
    //     axios.get('info/card-number')
    //         .then(res => setCardNumber(res.data.data))
    //         .catch(err => alert(err))
    // }, []);

    const onSubmit = values => {
        const {full_name, region, city, district, home, phone_number} = values;
        const formData = new FormData();
        // formData.append('file', file);
        formData.set('fullName', full_name);
        // formData.set('address', `${region} viloyati, ${city} shaxri, ${district} rayon, ${home}`);
        formData.set('phoneNumber', phone_number);

        axios.post('order', formData, {headers: {'Content-Type': 'multipart/form-data'}})

    };

    return (
        <div className="justify-body-center pt-md-5">
            <form onSubmit={handleSubmit(onSubmit)} className="form container">
                <div className="row">
                    <div className="col-12">
                        <p>
                            Buyurtma berish uchun formani to`ldiring va biz tez orada siz bilan bog`lanamiz.
                        </p>
                    </div>
                </div>
                <input className="form-control" type="text" placeholder="Ismingiz"
                       name={'full_name'}
                       aria-label=".form-control-sm example" ref={register({required: true})}/>
                {errors.full_name && <span>This field is required!</span>}
                {/*<input className="form-control mt-2" type="text" placeholder="Viloyat"*/}
                {/*       name={'region'}*/}
                {/*       ref={register({required: true})}*/}
                {/*       aria-label=".form-control-sm example"/>*/}
                {/*{errors.region && <span>This field is required!</span>}*/}
                {/*<input className="form-control mt-2" type="text" placeholder="Shaxar"*/}
                {/*       name={'city'}*/}
                {/*       ref={register({required: true})}*/}
                {/*       aria-label=".form-control-sm example"/>*/}
                {/*{errors.city && <span>This field is required!</span>}*/}
                {/*<input className="form-control mt-2" type="text" placeholder="Tuman"*/}
                {/*       name={'district'}*/}
                {/*       ref={register({required: true})}*/}
                {/*       aria-label=".form-control-sm example"/>*/}
                {/*{errors.district && <span>This field is required!</span>}*/}
                {/*<input className="form-control mt-2" type="text" placeholder="Uy manzili"*/}
                {/*       name={'home'}*/}
                {/*       ref={register({required: true})}*/}
                {/*       aria-label=".form-control-sm example"/>*/}
                {/*{errors.home && <span>This field is required!</span>}*/}
                <div className="input-group mt-2">
                    <div className="input-group-text" id="basic-addon1">+998</div>
                    <input type="text" className="form-control w-75" placeholder="YY XXX XX XX" aria-label="Username"
                           name={'phone_number'}
                           ref={register({required: true, minLength: 9})}
                           aria-describedby="basic-addon1"/>
                    {errors.phone_number && <span>This field is invalid!</span>}
                </div>
                {/*<div className="custom-file mt-2">*/}
                {/*    <input type="file" className="custom-file-input" id="validatedCustomFile"*/}
                {/*           onChange={(e) => setFile(e.target.files[0])} required/>*/}
                {/*    <label className="custom-file-label" htmlFor="validatedCustomFile">*/}
                {/*        {file ? file.name : 'Faylni tanlang...'}</label>*/}
                {/*</div>*/}

                <button className="btn btn-primary mt-2" type="submit">Buyurtma berish</button>
            </form>
        </div>
    );
};

export default Pay;
